public class Main {
    public static void main(String[] args) {
        User user = new User("RC", "Pery", 24, "Pasig City");
        Course course = new Course();

        course.setName("Java");
        course.setDescription("Intro to Java for JS Developers");
        course.setSeats(30);
        course.setFee(2000.00);
        course.setStartDate("25-04-2022");
        course.setEndDate("29-04-2022");
        course.setInstructor(user);

        System.out.println(String.format("User's first name: \n%s", user.getFirstName()));
        System.out.println(String.format("User's last name: \n%s", user.getLastName()));
        System.out.println(String.format("User's age: \n%s", user.getAge()));
        System.out.println(String.format("User's address: \n%s", user.getAddress()));

        System.out.println(String.format("Course's name: \n%s", course.getName()));
        System.out.println(String.format("Course's descriptions: \n%s", course.getDescription()));
        System.out.println(String.format("Course's seats: \n%s", course.getSeats()));
        System.out.println(String.format("Course's instructor's first name: \n%s", course.getInstructorName()));
    }
}